# Pow's Authentication

[![Pipeline](https://gitlab.com/mfaaach/story9/badges/master/pipeline.svg)](https://gitlab.com/mfaaach/story9/pipelines)
[![coverage report](https://gitlab.com/mfaaach/story9/badges/master/coverage.svg)](https://gitlab.com/mfaaach/story9/commits/master)


Fachri's PPW Lab 9 Project 

## URL

This lab projects can be accessed from [https://powaccount.herokuapp.com](https://powaccount.herokuapp.com)

## Authors

* **Muhammad Fachri Anandito** - [mfaaach](https://gitlab.com/mfaaach)
